# Complete the project and add typing
In the fighterService file, create a function to get detailed information for a fighter. Add the ability to view it. To do this, implement the getFighterInfo function, which would process a click on a fighter. Display the data using the showFighterDetailsModal function.   

The battle process will start automatically when the fighters are selected using the appropriate checkboxes. Create a fight function that takes both parameters and starts the game process. Players hit each other in turn, and their health level is reduced by getDamage. If one of them dies, then the game is over and the function should return the winner. The name of the winner is displayed using the showWinnerModal function.

## Implement functions:

-getDamage, calculating the opponent's health damage using the hitPower - blockPower formula;   
-getHitPower, calculating the impact power (the amount of damage to the opponent's health) according to the formula power = attack * criticalHitChance;, where criticalHitChance is a random number from 1 to 2;   
-getBlockPower, calculating the block strength (damping the opponent's blow) according to the formula power = defense * dodgeChance, where dodgeChance - a random number from 1 to 2. 
