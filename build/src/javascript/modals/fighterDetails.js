import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { getFighterDetails } from '../services/fightersService';
export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    showModal({ title, bodyElement });
}
export function createFighterDetails(fighter) {
    const name = fighter.name;
    const health = fighter.health;
    const attack = fighter.attack;
    const defense = fighter.defense;
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'p', className: 'fighter-name' });
    const healthElement = createElement({ tagName: 'p', className: 'fighter-health' });
    const attackElement = createElement({ tagName: 'p', className: 'fighter-attack' });
    const defenseElement = createElement({ tagName: 'p', className: 'fighter-defense' });
    getFighterDetails(fighter._id);
    nameElement.innerText = name;
    healthElement.innerText = `Health: ${health}`;
    attackElement.innerText = `Attack: ${attack}`;
    defenseElement.innerText = `Defense: ${defense}`;
    fighterDetails.append(nameElement, healthElement, attackElement, defenseElement);
    return fighterDetails;
}
