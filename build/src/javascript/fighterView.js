import { createElement } from './helpers/domHelper';
import { createFighterDetails } from './modals/fighterDetails';
export function createFighter(fighter, handleClick, selectFighter) {
    const { name, source } = fighter;
    const nameElement = createName(name);
    const imageElement = createImage(source, false);
    const checkboxElement = createCheckbox();
    const fighterContainer = createElement({ tagName: 'div', className: 'fighter' });
    fighterContainer.append(imageElement, nameElement, checkboxElement);
    const preventCheckboxClick = (ev) => ev.stopPropagation();
    const onCheckboxClick = (ev) => selectFighter(ev, fighter);
    const onFighterClick = (ev) => handleClick(ev, fighter);
    fighterContainer.addEventListener('click', onFighterClick, false);
    checkboxElement.addEventListener('change', onCheckboxClick, false);
    checkboxElement.addEventListener('click', preventCheckboxClick, false);
    return fighterContainer;
}
export function showFighterToFight(fighter, reverse) {
    const { name, source } = fighter;
    const nameElement = createName(name);
    const imageElement = createImage(source, reverse);
    const addInfoElement = createFighterDetails(fighter);
    const fighterContainer = createElement({ tagName: 'div', className: 'fighter' });
    fighterContainer.append(imageElement, nameElement, addInfoElement);
    return fighterContainer;
}
function createName(name) {
    const nameElement = createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;
    return nameElement;
}
function createImage(source, reverse) {
    const attributes = { src: source };
    const imgElement = createElement({ tagName: 'img', className: reverse ? 'fighter-image-invert' : 'fighter-image', attributes });
    return imgElement;
}
function createCheckbox() {
    const label = createElement({ tagName: 'label', className: 'custom-checkbox' });
    const span = createElement({ tagName: 'span', className: 'checkmark' });
    const attributes = { type: 'checkbox', value: '' };
    const checkboxElement = createElement({ tagName: 'input', attributes });
    label.append(checkboxElement, span);
    return label;
}
