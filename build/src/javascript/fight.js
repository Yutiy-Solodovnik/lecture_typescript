import { showModal } from './modals/modal';
import { showFighterToFight } from './fighterView';
import { createElement } from './helpers/domHelper';
import { showWinnerModal } from './modals/winner';
let firstFighterHealth;
let secondFighterHealth;
export function fight(firstFighter, secondFighter) {
    const firstFighterElement = showFighterToFight(firstFighter, false);
    const secondFighterElement = showFighterToFight(secondFighter, true);
    const fightLayer = createElement({ tagName: 'div', className: 'fight-layer' });
    fightLayer.append(firstFighterElement, secondFighterElement);
    firstFighterHealth = firstFighter.health;
    secondFighterHealth = secondFighter.health;
    showModal({ title: "FIGHT", bodyElement: fightLayer });
    const delay = (time) => {
        return new Promise((resolve) => setTimeout(resolve, time));
    };
    let interval1 = setInterval(() => {
        firstFighterElement.style.border = "none";
        secondFighterElement.style.border = "2px solid red";
        console.log("Hit1");
        if (Hit(firstFighter, secondFighter, fightLayer, 1) == true) {
            clearInterval(interval1);
            clearInterval(interval2);
            showWinnerModal(firstFighter);
            fightLayer.remove();
            resetHealth(firstFighter, secondFighter);
        }
    }, 1000);
    let interval2 = setInterval(() => delay(500).then(() => {
        secondFighterElement.style.border = "none";
        firstFighterElement.style.border = "2px solid red";
        console.log("Hit2");
        if (Hit(secondFighter, firstFighter, fightLayer, 0) == true) {
            clearInterval(interval1);
            clearInterval(interval2);
            showWinnerModal(secondFighter);
            fightLayer.remove();
            resetHealth(firstFighter, secondFighter);
        }
    }), 1000);
}
export function getDamage(attacker, enemy) {
    let damage = getHitPower(attacker) - getBlockPower(enemy);
    return (damage > 0) ? damage : 0;
}
export function getHitPower(fighter) {
    let criticalHitChance = getRandomChance(2);
    return fighter.attack * criticalHitChance;
}
export function getBlockPower(fighter) {
    let dodgeChance = getRandomChance(2);
    return fighter.defense * dodgeChance;
}
function setHealth(element, health, index) {
    element.getElementsByClassName("fighter-health")[index].innerHTML = `Health: ${health}`;
}
function getRandomChance(max) {
    return Math.floor(Math.random() * max) + 1;
}
function Hit(firstFighter, secondFighter, fighterContainer, index) {
    let health = secondFighter.health - getDamage(firstFighter, secondFighter);
    secondFighter.health = health;
    setHealth(fighterContainer, health, index);
    return (health <= 0) ? true : false;
}
function resetHealth(firstFighter, secondFighter) {
    firstFighter.health = firstFighterHealth;
    secondFighter.health = secondFighterHealth;
}
