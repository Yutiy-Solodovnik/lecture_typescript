import { createElement } from './helpers/domHelper'; 
import { createFighterDetails} from './modals/fighterDetails';

export function createFighter(fighter:FighterInfo, handleClick:any, selectFighter:any) {
  const { name, source} = fighter;
  const nameElement: HTMLElement = createName(name);
  const imageElement: HTMLElement = createImage(source, false);
  const checkboxElement: HTMLInputElement  = createCheckbox();
  const fighterContainer = createElement({ tagName: 'div', className: 'fighter' });
  
  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev: Event) => ev.stopPropagation();
  const onCheckboxClick = (ev: Event) => selectFighter(ev, fighter);
  const onFighterClick = (ev: Event) => handleClick(ev, fighter);

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick , false);

  return fighterContainer;
}

export function showFighterToFight (fighter: FighterInfo, reverse: boolean)
{
  const { name, source} = fighter;
  const nameElement: HTMLElement = createName(name);
  const imageElement: HTMLElement = createImage(source, reverse);
  const addInfoElement: HTMLElement = createFighterDetails(fighter);
  const fighterContainer = <HTMLInputElement>createElement({ tagName: 'div', className: 'fighter' });
  fighterContainer.append(imageElement, nameElement, addInfoElement);
  return fighterContainer;
}

function createName(name: string): HTMLElement {
  const nameElement = createElement({ tagName: 'span', className: 'name' });
  nameElement.innerText = name;

  return nameElement;
}

function createImage(source: string, reverse: boolean): HTMLElement {
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: reverse ? 'fighter-image-invert' : 'fighter-image', attributes });

  return imgElement;
}

function createCheckbox(): HTMLInputElement {
  const label = <HTMLInputElement>createElement({ tagName: 'label', className: 'custom-checkbox' });
  const span = <HTMLInputElement>createElement({ tagName: 'span', className: 'checkmark' });
  const attributes = { type: 'checkbox', value: '' };
  const checkboxElement: HTMLInputElement = <HTMLInputElement>createElement({ tagName: 'input', attributes });

  label.append(checkboxElement, span);
  return label;
}