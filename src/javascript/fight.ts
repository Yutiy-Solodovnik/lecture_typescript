import { showModal } from './modals/modal';
import { showFighterToFight } from './fighterView';
import { createElement } from './helpers/domHelper';
import { showWinnerModal } from './modals/winner';

let firstFighterHealth:number;
let secondFighterHealth:number;
export function fight(firstFighter: FighterInfo, secondFighter: FighterInfo):void {
  const firstFighterElement: HTMLElement = showFighterToFight(firstFighter, false);
  const secondFighterElement: HTMLElement = showFighterToFight(secondFighter, true);
  const fightLayer = createElement({ tagName: 'div', className: 'fight-layer' });
  fightLayer.append(firstFighterElement, secondFighterElement);
  firstFighterHealth = firstFighter.health;
  secondFighterHealth = secondFighter.health;
  showModal({title:"FIGHT", bodyElement:fightLayer});
  const delay = (time: number) => {
    return new Promise((resolve) => setTimeout(resolve, time))
  }
  let interval1 = setInterval(()=>
    {
      firstFighterElement.style.border = "none";
      secondFighterElement.style.border = "2px solid red";
      console.log("Hit1")
      if (Hit(firstFighter, secondFighter, fightLayer, 1) == true)
      {
        clearInterval(interval1);
        clearInterval(interval2);
        showWinnerModal(firstFighter);
        fightLayer.remove();
        resetHealth(firstFighter, secondFighter);
      }
    },1000)
   
  let interval2 = setInterval(() =>
  delay(500).then( ()=>
    {
      secondFighterElement.style.border = "none";
      firstFighterElement.style.border = "2px solid red";
      console.log("Hit2")
      if (Hit(secondFighter, firstFighter, fightLayer, 0) == true)
      {
        clearInterval(interval1);
        clearInterval(interval2);
        showWinnerModal(secondFighter);
        fightLayer.remove();
        resetHealth(firstFighter, secondFighter);
      }
    }),1000);
}

export function getDamage(attacker: FighterInfo, enemy: FighterInfo): number {
  let damage = getHitPower(attacker) - getBlockPower(enemy);
  return (damage > 0) ? damage : 0;
}

export function getHitPower(fighter: FighterInfo): number {
  let criticalHitChance = getRandomChance(2);
  return fighter.attack*criticalHitChance;
}

export function getBlockPower(fighter: FighterInfo): number {
  let dodgeChance = getRandomChance(2);
  return fighter.defense*dodgeChance;
}

function setHealth(element : HTMLElement,health:number, index:number): void
{
  element.getElementsByClassName("fighter-health")[index].innerHTML = `Health: ${health}`;
}

function getRandomChance(max:number): number
{
  return Math.floor(Math.random() * max) + 1;
}

function Hit(firstFighter: FighterInfo, secondFighter: FighterInfo, fighterContainer: HTMLElement, index: number): boolean
{
  let health = secondFighter.health - getDamage(firstFighter, secondFighter);
  secondFighter.health = health;
  setHealth(fighterContainer, health, index);
  return (health <= 0) ? true: false;
}

function resetHealth(firstFighter: FighterInfo, secondFighter: FighterInfo): void
{
  firstFighter.health = firstFighterHealth;
  secondFighter.health = secondFighterHealth;
}
