import { getFighters } from './services/fightersService'
import { createFighters } from './fightersView';

const rootElement: HTMLElement = <HTMLElement>document.getElementById('root');
const loadingElement: HTMLElement = <HTMLElement>document.getElementById('loading-overlay');

export async function startApp(): Promise<void> {
  try {
    loadingElement.style.visibility = 'visible';
    
    const fighters = await getFighters() as FighterInfo[];
    const fightersElement = createFighters(fighters);

    rootElement.appendChild(fightersElement);
  } catch (error) {
    console.warn(error);
    rootElement.innerText = 'Failed to load data';
  } finally {
    loadingElement.style.visibility = 'hidden';
  }
}
