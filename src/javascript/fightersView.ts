import { createFighter } from './fighterView';
import { showFighterDetailsModal} from './modals/fighterDetails';
import { getFighterDetails } from './services/fightersService'
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';

export function createFighters(fighters: FighterInfo[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements= fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event:Event, fighter:FighterInfo): Promise<void> {
  const fullInfo:FighterInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string): Promise<FighterInfo> {
  const fullFighterInfo: FighterInfo = await getFighterDetails(fighterId) as FighterInfo;
  return fullFighterInfo
}

function createFightersSelector() {
  const selectedFighters: Map<string, FighterInfo> = new Map();

  return async function selectFighterForBattle(event: Event, fighter:FighterInfo) {
    const fullInfo = await getFighterInfo(fighter._id);

    if ((event.target as HTMLInputElement).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      let [firstFighter, secondFighter] = selectedFighters.values();
      await fight(firstFighter, secondFighter);
    }
  }
}