import { showModal } from './modal';
import { createElement } from '../helpers/domHelper';

export  function showWinnerModal(fighter: FighterInfo):void {
  const attributes = { src: fighter.source };
  const winnerImage = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  showModal({title: `${fighter.name} wins`, bodyElement:winnerImage});
}
